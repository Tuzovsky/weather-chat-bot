# -*- coding: utf-8 -*-
import pandas as pd
from PIL import Image
import numpy
from io import BytesIO

class Drawer():
    def draw_from_list(self, data):
        gr = pd.Series(data).plot(kind='area', title=u'4 days weather')
        fig = gr.get_figure()
        img = self._fig2img(fig)
        bio = BytesIO()
        bio.name = 'image.jpeg'
        img.save(bio, 'PNG')
        bio.seek(0)
        return bio

    def _fig2data(self, fig):
        fig.canvas.draw()
        w, h = fig.canvas.get_width_height()
        buf = numpy.fromstring(fig.canvas.tostring_argb(), dtype=numpy.uint8)
        buf.shape = (w, h, 4)
        buf = numpy.roll(buf, 3, axis=2)
        return buf

    def _fig2img(self, fig):

        buf = self._fig2data(fig)
        w, h, d = buf.shape
        return Image.frombytes("RGBA", (w, h), buf.tostring())