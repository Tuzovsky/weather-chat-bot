# -*- coding: utf-8 -*-
import requests
import json
import datetime

class Weather:
    def __init__(self):
        self.geoloc_api_key = 'AIzaSyAUE0yRKfB55bjMt76OCE8t8ZrI3pohz78'
        self.weather_api_key = '4d2126e5b377c4c6c84421c79f6cdd15'

    def get_weather_text(self, data):
        if len(data[0]) == 0:
            data[0].append(u'москва')

        dates = data[1]
        if len(dates) == 0:
            now = datetime.date.today()
            for i in range(4):
                dates.append(now + datetime.timedelta(days=i))

        answer = u''
        self.results_t = []

        for city in data[0]:
            for date in dates:
                if date < datetime.date.today() or (date - datetime.date.today()).days > 9:
                    return u'Недопустимая дата для Яндекс Погоды: ' + date.strftime("%Y-%m-%d"), []
                location = self._define_city(city)
                weather = self._get_weather_by_location(location, date)
                answer += u'''В городе {0} на дату {1} следующая погода: \n{2}'''.format(city, date.strftime("%Y-%m-%d"), weather)

        return (answer, self.results_t)

    def _define_city(self, city):
        params = {'address': city, 'key': self.geoloc_api_key}
        response = requests.get('https://maps.googleapis.com/maps/api/geocode/json', params=params)

        return json.loads(response.content)['results'][0]['geometry']['location']

    def _get_weather_by_location(self, location, date):
        params = {'lang': 'ru_RU', 'lat': location['lat'], 'lon': location['lng'], 'hours': False}
        headers = {'X-Yandex-API-Key': '70b05c25-7f03-4b1f-b46d-ad21ba519295'}

        response = requests.get('https://api.weather.yandex.ru/v1/forecast',headers=headers,params=params).content

        parsed_responce = json.loads(response)
        weathers = [parsed_responce['fact']]
        for day in parsed_responce[u'forecasts']:
            weathers.append(day['parts']['day'])
        days = (date - datetime.date.today()).days
        weather = weathers[days]

        if days == 0:
            temp = 'temp'
        else:
            temp = 'temp_avg'
        self.results_t.append(weather[temp])

        result = u'Температура {0}.\n' \
               u'Скорость ветра {1}.\n' \
               u'Давление {2}\n'.format(weather[temp], weather['wind_speed'], weather['pressure_mm'])

        return result