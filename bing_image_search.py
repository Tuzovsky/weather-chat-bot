from azure.cognitiveservices.search.imagesearch import ImageSearchAPI
from azure.cognitiveservices.search.imagesearch.models import ImageType, ImageAspect, ImageInsightModule
from msrest.authentication import CognitiveServicesCredentials

class ImageSearcher():
    def __init__(self):
        self.subscription_key = "d0b9988faa4c4513ac608d32a8289c41"
        self.client = ImageSearchAPI(CognitiveServicesCredentials(self.subscription_key))

    def get_photo_url(self, text):
        image_results = self.client.images.search(
            query=text,
            image_type=ImageType.photo,
            aspect=ImageAspect.wide
        )
        return image_results.value[0].thumbnail_url
    