# -*- coding: utf-8 -*-
from ner import NerRecognizer
from weather import Weather
from bing_image_search import ImageSearcher
from draw_graphic import Drawer

from yandex_speech_client import speech_to_text
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

BOT_TOKEN = u'574948114:AAEQX7CBrS7fpT31Sm7fH3hAVDdf6-ER1CY'
wait_message_for_user = 'Ваш запрос обрабатывается. Пожалуйста, подожите.'

text_parser = NerRecognizer()
weather_finder = Weather()
image_searcher = ImageSearcher()

def start(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text= "Доброго времени суток!")

def help(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text= '''Бот поддерживает распознавание города, с приведением в 
    названия в нормальную форму. Отсылку картинок. Распознавание времени. Результата погоды текущего + 
    7 дней после(Яндекс Погода). Без указания времени отрисовывается график. По умолчанию - Москва(как в Я.П.) Может выдавать сразу для нескольких городов.
    ''')

def voice_message(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=wait_message_for_user)
    file = bot.getFile(update.message.voice.file_id)
    file_name = str(update.message.voice.file_id) + '.ogg'
    file.download(file_name)

    text_from_voice = speech_to_text(filename=file_name).encode('UTF-8')
    print text_from_voice

    ners = text_parser.get_ners(text_from_voice)

    is_without_dates = len(ners[1]) == 0
    answer, temps = weather_finder.get_weather_text(ners)
    if is_without_dates:
        bio = Drawer().draw_from_list(temps)
        bot.send_photo(update.message.chat_id, photo=bio)
    print answer

    photo_url = image_searcher.get_photo_url(ners[0][0])

    bot.send_photo(chat_id=update.message.chat_id, photo=photo_url)

    bot.send_message(chat_id=update.message.chat_id,
                     text=answer)


def text_message(bot, update):

    text = update.message.text.encode('utf8')
    print text

    ners = text_parser.get_ners(text)
    print ners

    is_without_dates = len(ners[1]) == 0
    answer, temps = weather_finder.get_weather_text(ners)
    print answer

    if is_without_dates:
        bio = Drawer().draw_from_list(temps)
        bot.send_photo(update.message.chat_id, photo=bio)

    photo_url = image_searcher.get_photo_url(ners[0][0])

    bot.send_photo(chat_id=update.message.chat_id, photo=photo_url)

    bot.send_message(chat_id=update.message.chat_id,
                    text=answer)

updater = Updater(token = BOT_TOKEN)

start_handler = CommandHandler('start', start)
help_handler = CommandHandler('help', help)

updater.dispatcher.add_handler(start_handler)
updater.dispatcher.add_handler(help_handler)
updater.dispatcher.add_handler(MessageHandler(Filters.voice, voice_message))
updater.dispatcher.add_handler(MessageHandler(Filters.text, text_message))
updater.start_polling()