# -*- coding: utf-8 -*-
import pymorphy2
import pandas as pd
from dateparser import parse
import string
from pyaspeller import Word

class NerRecognizer():
    def __init__(self):
        #self.extractor = DatesExtractor()
        self.morph = pymorphy2.MorphAnalyzer()
        self.cities = map(lambda x: x.decode('utf8').lower(), list(pd.read_csv('city.csv', sep=';').name))

    def _get_norm_word(self, word):
        word = word.translate(None, string.punctuation.replace('-',''))
        is_correct = Word(word)
        if not is_correct:
            word = is_correct.variants[0].encode('utf-8')
        parse_result = self.morph.parse(word.decode('utf-8'))[0]
        return parse_result.normal_form

    def get_ners(self, text):
        return (self._recognize_city(text), self._recognize_date(text))

    def _recognize_city(self, text):
        words = [self._get_norm_word(x) for x in text.lower().split(' ')]
        cities = [word for word in words if word in self.cities]

        return cities

    def _recognize_date(self, text):
        res = []
        text = text.translate(None, string.punctuation).lower()
        for i in text.split(' '):
            parsed = parse(i)
            if parsed:
                res.append(parsed.date())
        return res
